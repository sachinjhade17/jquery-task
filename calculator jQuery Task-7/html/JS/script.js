$(document).ready(function () {

    var input = $("#getInput");

    function calculation() {
        try {
            // Eval funtion for calculation        
            result = eval(input.val());
            $("#getInput").val(result);
        }
        catch (error) {
            //  It will show the error for the 2 sec                                                
            input.val(error);
            setTimeout(function () { $("#getInput").val("") }, 2000);
        }
    }

    // Click event for all inputs
    $(document).on('click', "input[type='button']", function () {
        inputValue = $(this).val();
        input.val(input.val() + inputValue);
    })


    // When press on Equal button
    $(document).on("click", "#equalBtn", function () {
        calculation()
    })
    // When press on Square Button
    $(document).on("click", "#square ", function () {
        square = eval(input.val() * input.val());
        input.val(square);
    })

    // When press on button
    $(document).on("click", "#backSpace", function () {
        input.val($("#getInput").val().substring(0, $("#getInput").val().length - 1));
    })

    // All Clear Button
    $(document).on("click", "#clear", function () {
        input.val("")
    })

    //Bind keypress event to document

    var keyDict = {
        "48": "0",
        "49": "1",
        "50": "2",
        "51": "3",
        "52": "4",
        "53": "5",
        "54": "6",
        "55": "7",
        "56": "8",
        "57": "9",
        "42": "*",
        "43": "+",
        "45": "-",
        "46": ".",
        "47": "/",
        "37": "%",
        "40": "(",
        "41": ")",
        "61": "=",

    }


    // When press on Backspace
    $(document).keydown(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '8') {
            $("#backSpace").trigger("click")
        }
    });

    $(document).keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        // When press on enter
        if (keycode == '13') {
            calculation()
        }
        // All numbers, expression and point
        else if (keyDict.hasOwnProperty(keycode)) {
            input.val($("#getInput").val() + keyDict[keycode])
        }
    });
})



