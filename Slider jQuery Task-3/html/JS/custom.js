$(document).ready(function () {
    $("#on").attr("disabled", true).css({ "background-color": "gray", "color": "white" });

    $(document).on('click', '#next', function () {                          // Click event on Next for next Images
        if ($('.slider-images.active').index() >= $('.slider-images').length - 1) {
            $(".slider-images").removeClass("active").first().addClass("active");
        } else {
            $('.slider-images.active').removeClass('active').next().addClass('active');      
        }
        $('.dot').removeClass('activeForDot').eq($('.slider-images.active').index()).addClass('activeForDot');
    });
    $(document).on('click', '#prev', function () {                          // Click event on Prev for next Images
        if ($('.slider-images.active').index() == 0) {
            $(".slider-images").removeClass("active").last().addClass("active");
        } else {
            $('.slider-images.active').removeClass('active').prev().addClass('active');
        }
        $('.dot').removeClass('activeForDot').eq($('.slider-images.active').index()).addClass('activeForDot');
    });
    var autoslider = setInterval(function () {                               // Time Interval for Autoslider          
        $('#next').trigger('click');
    }, 2000);
    $(document).on('click', '.dot', function () {
        $('.slider-images').removeClass('active').eq($(this).index()).addClass('active');
        $(this).addClass('activeForDot').siblings().removeClass("activeForDot");
    });
    $(document).on('click', '#off', function () {                         // Pause Buttton for Pause the Auto Sliding       
        clearInterval(autoslider);
        $("#on").attr("disabled", false).css({ "background-color": "navy", "color": "white" });
    });
    $(document).on('click', '#on', function () {                      // ON Buttton to continue Auto Sliding                       
        autoslider = setInterval(function () {
            $('#next').trigger('click');
            $("#on").attr("disabled", true).css({ "background-color": "gray", "color": "white" });
            console.log("on");
        }, 2000);
    });

});
