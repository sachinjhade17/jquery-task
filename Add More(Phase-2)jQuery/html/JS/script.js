$(document).ready(function () {

    var firstChild, secondChild, parentIndex, parentHeader, childParent, childLength;
    // Add More Button                                                                                                                                                                                                                          
    $(document).on("click", ".addMoreBtn", function () {
        $(".cloneData").append($(".parent-content").first().clone().removeClass("hide").removeAttr("id"))
        $(".parent-input").trigger("keyup")

    });
    // Add Child Button to add new child
    $(document).on("click", ".addChildBtn", function () {
        $(this).closest(".parent-content").append($(".child").first().clone().removeClass("hide"))

    });
    // Remove Child Button to remove child
    $(document).on("click", ".removeBtn", function () {
        confirmDelete = $(this).closest(".child")
        bootbox.confirm({
            message: "Are you sure ?",
            callback: function (result) {
                if (result == true) {
                    $(".myTable").eq(parentIndex).remove();
                    confirmDelete.remove();
                    $(".parent-input").trigger("keyup");
                }
            }
        });

    });

    // Remove Parent Button to remove Parent
    $(document).on("click", ".removeParentBtn", function () {
        confirmDelete = $(this)
        var parentIndex = $(this).parent().index();
        bootbox.confirm({
            message: "Are you sure ?",
            callback: function (result) {
                if (result == true) {
                    $(".myTable").eq(parentIndex).remove();
                    confirmDelete.closest(".parent-content").remove();
                    $(".parent-input").trigger("keyup");
                }
            }
        });
    });

    // Trigeer to Keyup child-1 input with Parent
    $(document).on("keyup", ".childInput", function () {
        $(".parent-input").trigger("keyup")
    })

    // Trigeer to Keyup child-2 input with Parent
    $(document).on("keyup", ".childInput1", function () {
        $(".parent-input").trigger("keyup")
    })


    // Keyup parent to show anything in the table at the same time that character will be added in the SPECIFICATION part.

    $(document).on("keyup", ".parent-input", function () {
        var parentIndex = $(this).parent().index();
        console.log(parentIndex, "parentIndex");                     // Parent index
        parentHeader = $(this).closest('.parent-content');
        childParent = $(this).parent().find(".child");
        childLength = $(childParent).length;
        var tableString, updateString;

        if ($(".myTable").eq(parentIndex).length > 0) {              // Condition to Update the table
            $(".myTable").eq(parentIndex).html("");
            updateString = '<tr>'

            updateString += '<tr><th>' + $(parentHeader).find('.parent-input').val() + '</th></tr>'

            for (var i = 0; i < childLength; i++) {
                firstChild = $(childParent).eq(i).find(".childInput").val()
                secondChild = $(childParent).eq(i).find(".childInput1").val()
                updateString += '<tr><td>' + firstChild + '</td><td>' + secondChild + '</td></tr>'
            }

            $(".myTable").eq(parentIndex).append(updateString)         //Updated string to append the updated value in table 
            updateString += `</tr>`

        } else {
            // Create table and append the value in it          
            tableString = '<table class="myTable">'
            tableString += '<tr><th>' + $(parentHeader).find('.parent-input').val() + '</th></tr>'

            for (var i = 0; i < childLength; i++) {
                firstChild = $(childParent).eq(i).find(".childInput").val()
                secondChild = $(childParent).eq(i).find(".childInput1").val()
                tableString += '<tr><td>' + firstChild + '</td><td>' + secondChild + '</td></tr>'
            }
            tableString += '</table>';
            $(".tableContent").append(tableString);

        }

    });

})




