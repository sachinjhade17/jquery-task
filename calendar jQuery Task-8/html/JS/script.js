$(document).ready(function () {

    // Array of Months
    monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    var date = new Date();
    var currentMonth = date.getMonth();
    var currentYear = date.getFullYear();

    // For loop to get Months value 
    for (i = 0; i <= monthName.length - 1; i++) {
        $("#months").append($('<option />').val(i).html(monthName[i]));
    }

    // For loop to get years value 
    for (i = 1970; i <= 2070; i++) {
        $("#years").append($('<option />').val(i).html(i));
    }


    // Initial Call Calendar function to display on screen
    calendar(currentMonth, currentYear);

    //  Click Events 
    $(document).on("click", ".leftArrow", function () {
        previous()
    })
    $(document).on("click", ".rightArrow", function () {
        next()
    })
    $(document).on("click", "#findBtn", function () {
        findDate()
    })
    $(document).on("click", "#todayBtn", function () {
        location.reload();          //simple refresh    
    })

    $('#months').change(function () {
        findDate()
    });

    // Next function to see next month
    function next() {
        currentYear = (currentMonth === 11) ? currentYear + 1 : currentYear;
        currentMonth = (currentMonth + 1) % 12;
        $(".tableRow").remove();
        calendar(currentMonth, currentYear);
        $(".currentMonthYear").html(monthName[currentMonth] + " - " + currentYear);
    }

    // Previous function to see next month
    function previous() {
        currentYear = (currentMonth === 0) ? currentYear - 1 : currentYear;
        currentMonth = (currentMonth === 0) ? 11 : currentMonth - 1;
        $(".tableRow").remove();
        calendar(currentMonth, currentYear);
        $(".currentMonthYear").html(monthName[currentMonth] + " - " + currentYear)
    }


    // calendar function 
    function calendar(month, year, searchDate) {
        month = parseInt(month)
        year = parseInt(year)

        daysInMonth = new Date(year, month + 1, 0).getDate();
        let firstDay = (new Date(year, month)).getDay();
        var currentDate = new Date().getDate();


        // For loop to get Dates value 
        for (i = 1; i <= daysInMonth; i++) {
            $("#dates").append($('<option  />').val(i).html(i));
        }

        // For loop to append Rows in table
        for (i = 0; i < 6; i++) {
            tableRow = '<tr class= "tableRow">'
            for (j = 1; j <= 7; j++) {
                if (i == 0 && j <= firstDay) {
                    tableRow += "<td>" + " " + "</td>"
                }
                else {
                    date = (j + (7 * i)) - firstDay;
                    // condition to Highlight the current Date
                    if (date == currentDate && month == new Date().getMonth() && year == new Date().getFullYear()) {
                        tableRow += "<td class='tableData bgTodysDate'>" + currentDate + "</td>"
                        console.log(date, currentDate, "currentDate");
                    }
                    // condition to Highlight the Searched Date
                    else if (date == searchDate && month == currentMonth && year == currentYear) {
                        tableRow += "<td class='tableData bgFindDate'>" + searchDate + "</td>"
                    }
                    else if (date <= daysInMonth) {
                        tableRow += "<td class=tableData>" + date + "</td>"
                    }
                }
                $(".currentMonthYear").html(monthName[currentMonth] + " - " + currentYear)
            }
            $("table").append(tableRow);
            tableRow = "<tr/>"
        }
    }
    // function to find the search date 
    function findDate() {
        $(".tableRow").remove();
        searchDate = $("#dates option:selected").val();
        currentYear = $("#years option:selected").val();
        currentMonth = $("#months option:selected").val();
        searchDate = parseInt(searchDate)

        $('#dates').empty();
        calendar(currentMonth, currentYear, searchDate);
    }
})



