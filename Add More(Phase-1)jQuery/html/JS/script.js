$(document).ready(function () {

    var firstChild, secondChild;
    // Add More Button
    $(document).on("click", ".addMoreBtn", function () {
        $(".cloneData").append($(".parent-content").first().clone().removeClass("hide").removeAttr("id"))

    });
    // Add Child Button to add new child
    $(document).on("click", ".addChildBtn", function () {
        $(this).closest(".parent-content").append($(".child").first().clone().removeClass("hide"))
    });
    // Remove Child Button to remove child
    $(document).on("click", ".removeBtn", function () {
        $(this).closest(".child").remove();
    });


    $(document).on("click", ".submitBtn", function () {
        var parentIndex = $(this).parent().index();                      // Parent index
        var parentHeader = $(this).closest('.parent-content');
        var childParent = $(this).parent().find(".child");
        var childLength = $(childParent).length;
        var tableString, updateString;


        if ($(".myTable").eq(parentIndex).length > 0) {              // Condition to Update the table

            $(".myTable").eq(parentIndex).html("");
            updateString = '<tr>'
            updateString += '<tr><th>' + $(parentHeader).find('.parent-input').val() + '</th></tr>'

            for (var i = 0; i < childLength; i++) {
                firstChild = $(childParent).eq(i).find(".childInput").val()
                secondChild = $(childParent).eq(i).find(".childInput1").val()
                updateString += '<tr><td>' + firstChild + '</td><td>' + secondChild + '</td></tr>'
            }
            $(".myTable").eq(parentIndex).append(updateString)         //Updated string to append the updated value in table 
            updateString += `</tr>`

        } else {
            // Create table and append the value in it
            tableString = '<table class="myTable">'
            tableString += '<tr><th>' + $(parentHeader).find('.parent-input').val() + '</th></tr>'

            for (var i = 0; i < childLength; i++) {
                firstChild = $(childParent).eq(i).find(".childInput").val()
                secondChild = $(childParent).eq(i).find(".childInput1").val()
                tableString += '<tr><td>' + firstChild + '</td><td>' + secondChild + '</td></tr>'
            }
            tableString += '</table>';
            $(".tableContent").append(tableString);

        }

    });


})

