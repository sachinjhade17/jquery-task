$(document).ready(function () {

    var table = $("#myTable").DataTable();
    var firstName, lastName, gender, email, mobileNumber, dateOfBirth, favoriteSport, about, checkBox, hours, zipCode, IPaddresss;
    var currentRow = null;
    $("#cancel").hide();


    $(document).on("click", "#saveNextBtn", function (e) {                      // NEXT Button 
        e.preventDefault();
        firstName = $("#firstName").val();
        lastName = $("#lastName").val();
        gender = $("input[name='gender']:checked").val();
        hours = $("#hours").val();
        zipCode = $("#zipCode").val();
        IPaddresss = $("#ipinput").val();

        if ($("#myform").valid() == true) {
            nextStep();
        }
    })

    $(document).on("click", "#saveNextSecondBtn", function (e) {                 // SAVE & NEXT Button 
        e.preventDefault();
        email = $("#emailId").val();
        mobileNumber = $("#mobileNumber").val();
        dateOfBirth = $("#datepicker").val();

        if ($("#myform").valid() == true) {
            nextStep();
        }
    })

    $(document).on("click", "#prevbtn", function () {                            // PREV Button 
        prevStep();
    })

    // SUBMIT Button 
    $(document).on("click", "#submitBtn", function (e) {
        e.preventDefault();                                                         // To stop Auto refresh the page 
        favoriteSport = $('#favoriteSport option:selected').val();
        about = $("#txtarea").val();
        checkBox = $('#agree').is(":checked");

        if ($("#myform").valid() == true) {
            var currentData = [firstName, lastName, gender, hours, zipCode, IPaddresss, email,
                mobileNumber, dateOfBirth, favoriteSport, about, checkBox,
                '<button id="edit"> Edit</button>',
                '<button id="delete">Delete</button>']
            redirectOnFirstPage();

            if (currentRow) {                                                  // if Condition for UPDATE Button (for update the data)
                table.row(currentRow).data(currentData).draw();
                currentRow = null;
                $('#agree').attr('checked', false);
                $("#submitBtn").html("SUBMIT");
                $("#saveNextSecondBtn").html("SAVE & NEXT");
            }
            else {
                table.row.add(currentData).draw(false);                          // It will add new data in the data table
            }
        }
    })


    $(document).on("click", "#cancel", function () {               // Cancel Button 
        redirectOnFirstPage();
    })

    $(document).on("click", "#delete", function () {                // Delete Button 
        table.row($(this).parents('tr')).remove().draw();
        redirectOnFirstPage();
        currentRow = null;
    })

    $(document).on("click", "#edit", function () {                    // Edit Button

        $("#saveNextSecondBtn").html("UPDATE & NEXT");
        currentRow = $(this).parents('tr');

        gender = $(this).closest('tr').find('td:eq(2)').text();

        $('#firstName').val(currentRow.find('td:eq(0)').text())
        $('#lastName').val(currentRow.find('td:eq(1)').text())
        $(`#${gender}`).prop('checked', true);
        $("#hours").val(currentRow.find('td:eq(3)').text())
        $("#zipCode").val(currentRow.find('td:eq(4)').text())
        $("#ipinput").val(currentRow.find('td:eq(5)').text())
        $("#emailId").val(currentRow.find('td:eq(6)').text())
        $("#mobileNumber").val(currentRow.find('td:eq(7)').text())
        $("#datepicker").val(currentRow.find('td:eq(8)').text())
        $("#favoriteSport").val(currentRow.find('td:eq(9)').text())
        $("#txtarea").val(currentRow.find('td:eq(10)').text())
        $('#agree').attr('checked', true);


        $("#cancel").show();
        $("#submitBtn").html("UPDATE");
    })

    function nextStep() {               // Redirect on Next Page
        $('.ip-form.activeContent').removeClass('activeContent').next().addClass('activeContent');
        $(".stepbtn.active").removeClass('active').next().addClass('active');
    }

    function prevStep() {               // Redirect on Previous Page
        $('.ip-form.activeContent').removeClass('activeContent').prev().addClass('activeContent');
        $(".stepbtn.active").removeClass('active').prev().addClass('active');
    }

    function redirectOnFirstPage() {        // Redirect on First Page
        $(".ip-form").removeClass("activeContent").first().addClass("activeContent").siblings().removeClass("activeContent");
        $(".wizard-form .stepbtn").removeClass("active").first().addClass("active").siblings().removeClass("active");
        $('#myform')[0].reset();
        $("#cancel").hide();
        $("#submitBtn").html("SUBMIT");
        $("#saveNextSecondBtn").html("SAVE & NEXT");
        $('#agree').attr('checked', false);

    }






    // IP Address PlugIn and masking

    $(function () {
        $('#ipinput').IpInput({
            ColorTrue: "blue",
            ColorFalse: "red",
        });
    });


    // Masking For All 

    $('#ipinput').mask('099.099.099.099', { placeholder: "___.___.___.___" });
    $('#zipCode').mask('099-999');
    $('#hours').mask('00');
    $('#datepicker').mask('00/00/0000');
    $('#mobileNumber').mask('0000000000');



    //  Date picker PlugIn

    $(function () {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1980:2037',
            dateFormat: 'dd/mm/yy',
        });
    });

    // Validation Plugin

    $("#myform").validate({
        rules: {
            firstName: "required",
            lastName: "required",
            email: "required",
            mobileNumber: "required",
            sports: "required",


            gender: {
                required: true
            },
            zipCode: {
                required: true,
                minlength: 7,
            },
            ipinput: {
                required: true,
                minlength: 11,
            },
            dateOfBirth: {
                required: true,
                minlength: 10,
            },

            mobileNumber: {
                required: true,
                minlength: 10,
                maxlength: 10,
                digits: true,
            },

            hours: {
                required: true,
                min: 3,
                max: 18

            },
            about: {
                required: true,
                minlength: 10,
            },

            agree: {
                required: true,
            },
        },
        messages: {
            lastName: "please enter your last name...",
            agree: "Please read and check the T&C!",
            gender: "Please select  your gender.!",

            hours: {
                min: "Please enter more than 4 hours",
                max: "Superman..! You can work maximum 18 hours "
            },
            zipCode: {
                minlength: "Please enter 6 digit PinCode number!"
            },
            ipinput: {
                minlength: "Please enter valid IP !"
            },
            agree: {
                agree: "Please read and check the T&C!"
            },
            mobileNumber: {
                minlength: "please enter 10 digit number ",
            },

            dateOfBirth: {
                minlength: "Please enter valid date ",
            },
        }
    });
});

