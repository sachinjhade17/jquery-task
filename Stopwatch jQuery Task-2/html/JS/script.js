
$(document).ready(function () {
  var counter = Csec = sec = min = hrs = totalSeconds = 0;           // Globle variables declaration

  $(document).on("click", "#start", function () {                    // Start Button to start stopwatch
    var status = $("#start").text();

    if (status == "START") {
      counter = setInterval(interval, 10);
      logAndStatus("START", "green");
      $("#start").attr("disabled", true).css({ color: "gray", "border-color": "gray" });    // After one click the start button will be disable
      $("#pause").attr("disabled", false).css({ color: "darkcyan", "border-color": " darkcyan" });
      $("#stop").attr("disabled", false).css({ color: "red", "border-color": " red" });

    } else if (status == "RESUME") {
      logAndStatus("RESUME", "purple");
      counter = setInterval(interval, 10);
      $("#start").attr("disabled", true).html("START").css({ color: "gray", "border-color": "gray" });
      $("#pause").attr("disabled", false).css({ color: "darkcyan", "border-color": " darkcyan" });
      $("#stop").attr("disabled", false).css({ color: "red", "border-color": " red" });

    } else if (status == "RESTART") {
      $("#start").attr("disabled", true).html("START").css({ color: "gray", "border-color": "gray" });
      $("#pause").attr("disabled", false).css({ color: "darkcyan", "border-color": " darkcyan" });
      $("#stop").attr("disabled", false).css({ color: "red", "border-color": " red" });
      logAndStatus("RESTART", "blue");
      resetData();
      counter = setInterval(interval, 10);
    }
  });

  function calculation(totalSeconds) {                   // Time conversion to Hours,minutes and seconds

    hrs = Math.floor(totalSeconds / 3600);
    min = Math.floor((totalSeconds % 3600) / 60);
    sec = totalSeconds - hrs * 3600 - min * 60;

    hrs = hrs < 10 ? "0" + hrs : hrs;
    min = min < 10 ? "0" + min : min;
    sec = sec < 10 ? "0" + sec : sec;

    $("#hours").html(hrs);
    $("#min").html(min);
    $("#sec").html(sec);
  }

  function interval() {                                   // Time Interval function
    $("#cent-sec").text(Csec);
    Csec++;
    if (Csec > 99) {
      totalSeconds++;
      calculation(totalSeconds);
      Csec = 0;
    }
  }

  $("#pause").click(function () {                         // Event for Pause stopwatch
    $("#start ").attr("disabled", false).css({ color: "green", "border-color": " green" });
    $("#pause").attr("disabled", true).css({ color: "gray", "border-color": " gray" });
    logAndStatus("PAUSE", "darkcyan");
    clearInterval(counter);
    $("#start").html("RESUME").attr("data-status", "RESUME").css({ color: "purple", "border-color": " purple" });;
  });

  $("#stop").click(function () {                         // Event for Stop Stopwatch
    $("#start ").attr("disabled", false).css({ color: "green", "border-color": " green" });
    $("#stop").attr("disabled", true).css({ color: "gray", "border-color": " gray" });
    clearInterval(counter);
    $("#start").html("RESTART").attr("data-status", "RESTART").css({ color: "blue", "border-color": " blue" });
    logAndStatus("STOP", "red");
  });

  $("#reset").click(function () {                        // Event for Reset Stopwatch
    resetData();
    $("ol").empty();
    $("#statusaDemo").html("Click on start button to start Stopwatch...").css({ color: "black", border: "1px solid gray" });
    $("#start ").attr("disabled", false).css({ color: "green", "border-color": " green" });
    $("#start").html("START").attr("data-status", "START");
  });

  function resetData() {                                 // function  to reset the data 
    hrs = min = sec = totalSeconds = 0;
    $("#hours, #min, #sec, #cent-sec").text("00");
    clearInterval(counter);
  }

  function logAndStatus(logs, colorOfBorder) {           // Function for logs, Status and color
    $("ol").append("<li>" + logs + " AT:- Hours :-" + hrs + " Minutes:-" + min + " Seconds:-" + sec + "</li>");
    $("#statusaDemo").html(`${logs}  AT :- ${hrs} HH:: ${min} MM:: ${sec} SS`).css({ color: colorOfBorder, "border-color": colorOfBorder });
  }

});
